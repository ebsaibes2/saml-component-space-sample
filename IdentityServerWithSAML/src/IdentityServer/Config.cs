﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using IdentityServer4.Models;
using System.Collections.Generic;
using System.Security.Cryptography;
using IdentityModel;
using IdentityServer4;
using IdentityServer4.EntityFramework.Entities;
//using IdentityServer4.EntityFramework.Entities;
using ApiResource = IdentityServer4.Models.ApiResource;
using Client = IdentityServer4.Models.Client;
using IdentityResource = IdentityServer4.Models.IdentityResource;
using Secret = IdentityServer4.Models.Secret;
namespace IdentityServer
{
    public static class Config
    {
        private static string mapCollectionScope = "mapcollection";

        public static IEnumerable<IdentityResource> Ids =>
            new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile()
                {
                    UserClaims = new List<string>()
                    {
                        JwtClaimTypes.PreferredUserName,
                        JwtClaimTypes.Name,
                        JwtClaimTypes.FamilyName,
                        "mapcollection-user"
                    }
                },
            };
        

        public static IEnumerable<ApiResource> Apis =>
            new ApiResource[]
            {
                new ApiResource("api1", "My Api"),
                new ApiResource(mapCollectionScope, "Map Collection Sites Resource")
                {
                    ApiSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    UserClaims =
                    {
                        JwtClaimTypes.Subject,
                        JwtClaimTypes.Name,
                        JwtClaimTypes.FamilyName,
                        JwtClaimTypes.PreferredUserName,
                        "mapcollection-user"
                    }
                }
            };

        public static IEnumerable<Client> Clients =>
            new List<Client>
            {
                new Client
                {
                    ClientId = "client",
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedScopes = {"api1"}
                },
                new Client
                {
                    ClientId = "mvc",
                    ClientSecrets = { new Secret("secret".Sha256()) },

                    AllowedGrantTypes = GrantTypes.Code,
                    RequireConsent = false,
                    RequirePkce = true,

                    // where to redirect to after login
                    RedirectUris = { "http://localhost:5002/signin-oidc" },

                    // where to redirect to after logout
                    PostLogoutRedirectUris = { "http://localhost:5002/signout-callback-oidc" },

                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile
                    }
                },
                new Client
                {
                    ClientId = "MapCollectionSites",
                    ClientSecrets = { new Secret("secret".Sha256()) },

                    AllowedGrantTypes = GrantTypes.Hybrid,
                    RequireConsent = false,
                    RequirePkce = false,
                    //RefreshTokenUsage = TokenUsage.OneTimeOnly,
                    //RefreshTokenExpiration = TokenExpiration.Sliding,

                    // where to redirect to after login
                    RedirectUris = { "https://localhost:44378/signin-oidc" },

                    // where to redirect to after logout
                    PostLogoutRedirectUris = { "https://localhost:44378/signout-callback-oidc" },

                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                    },
                },
                new Client
                {
                    ClientId = "Catalog",

                    AllowedGrantTypes = GrantTypes.Hybrid,

                    RequireConsent = false,
                    
                    // where to redirect to after login
                    RedirectUris = { "https://localhost:44328/" },

                    // where to redirect to after logout
                    PostLogoutRedirectUris = { "https://localhost:44328/" },

                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },

                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        mapCollectionScope,
                    },
                },
            };
    }
}
