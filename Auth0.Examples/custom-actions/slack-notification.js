const initSlackNotify = require('slack-notify');

exports.onExecutePostLogin = async (event, api) => {
  const slack = initSlackNotify(event.secrets.SLACK_WEBHOOK_URL);
    const message = `User Login: ${event.user.email}`;
    slack.success({
      text: message
    });
};
