function (user, context, callback) {
    const assignedRoles = (context.authorization || {}).roles;
    const idTokenClaims = context.idToken || {};
    
    //Notice that the key matches the claim type in the client application
    idTokenClaims['https://schemas.dev-5d7g1hqj.com/roles'] = assignedRoles;
  
    callback(null, user, context);
  }