﻿namespace WebApp.MVC.Models
{
    public class UserProfileViewModel
    {
        public string EmailAddress { get; set; }

        public string Name { get; set; }

        public string ProfileImage { get; set; }
        
        public string Role { get; set; }
    }
}