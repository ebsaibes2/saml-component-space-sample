﻿using Microsoft.Extensions.Configuration;

namespace WebApp.MVC.Constants
{
    public class AuthenticationVariables
    {
        public static class Constants
        {
            public const string Auth0 = "Auth0";
            public const string CallBackPath = "/callback";
            public const string Tenant = "dev-5d7g1hqj";
        }

        public static class CustomClaims
        {
            public static string Roles = $"https://schemas.{Constants.Tenant}.com/roles";
        }
    }
}