﻿namespace Headspring.LocalDevSamlIdentityProvider
{
    using Microsoft.AspNetCore;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Logging;
    using Serilog;
    using Serilog.Filters;

    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureLogging((hostingContext, logging) =>
                {
                    logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                    logging.AddConsole();
                    logging.AddDebug();
                    logging.AddSerilog(new LoggerConfiguration()
                        .MinimumLevel.Debug()
                        .WriteTo.RollingFile("Logs/saml-{Date}.log")
                        .Filter.ByIncludingOnly(Matching.FromSource("ComponentSpace.Saml2"))
                        .CreateLogger());
                })
                .UseStartup<Startup>();
    }
}
