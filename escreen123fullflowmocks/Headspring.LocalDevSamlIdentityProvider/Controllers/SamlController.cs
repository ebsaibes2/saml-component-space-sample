﻿using Microsoft.AspNetCore.Identity;

namespace Headspring.LocalDevSamlIdentityProvider.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using ComponentSpace.Saml2;
    using ComponentSpace.Saml2.Assertions;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [Route("[controller]/[action]")]
    public class SamlController : Controller
    {
        private readonly ISamlIdentityProvider _samlIdentityProvider;
        private readonly SignInManager<IdentityUser> _signInManager;

        public SamlController(ISamlIdentityProvider samlIdentityProvider,
            SignInManager<IdentityUser> signInManager)
        {
            _samlIdentityProvider = samlIdentityProvider;
            _signInManager = signInManager;
        }

        [Authorize]
        public async Task<IActionResult> InitiateSingleSignOn(string target)
        {
            var tenantId = "idpfakesso";
            if(!string.IsNullOrWhiteSpace(target))
                target = tenantId + "/" + target;

            await _samlIdentityProvider.SetConfigurationIDAsync(tenantId);

            var userName = User.Identity.Name;

            var attributes = SetConventionalSamlAttributes(userName, target, "IDP_Initiated");

            var partnerName = "urn:AppServiceProvider:FedGateway";

            //See https://stackoverflow.com/questions/34350160/what-is-exactly-relaystate-parameter-used-in-sso-ex-saml
            //There is also another, de facto standard use for RelayState when using Idp-initiated log on. In that case, there is no incoming request from the SP, so there can be no state to be relayed back.Instead, the RelayState is used by the IDP to signal to the SP what URL the SP should redirect to after successful sign on. That is not part of the SAML2 standard.
            var relayState = target ?? "/";

            // Initiate single sign-on to the service provider (IdP-initiated SSO)
            // by sending a SAML response containing a SAML assertion to the SP.
            // The optional relay state normally specifies the target URL once SSO completes.
            await _samlIdentityProvider.InitiateSsoAsync(partnerName, userName, attributes, relayState);

            return new EmptyResult();
        }

        public async Task<IActionResult> InitiateSingleLogout(string returnUrl = null)
        {
            // Request logout at the identity provider.
            await _samlIdentityProvider.InitiateSloAsync(relayState: returnUrl);

            return new EmptyResult();
        }

        public async Task<IActionResult> SingleSignOnService()
        {
            var tenantId = "idpfakesso";
            await _samlIdentityProvider.SetConfigurationIDAsync(tenantId);

            // Receive the authn request from the service provider (SP-initiated SSO).
            await _samlIdentityProvider.ReceiveSsoAsync();

            // If the user is logged in at the identity provider, complete SSO immediately.
            // Otherwise have the user login before completing SSO.
            if (User.Identity.IsAuthenticated)
            {
                await CompleteSsoAsync();

                return new EmptyResult();
            }
            else
            {
                return RedirectToAction("SingleSignOnServiceCompletion");
            }
        }

        public async Task<IActionResult> SingleLogoutService()
        {
            // Receive the single logout request or response.
            // If a request is received then single logout is being initiated by a partner service provider.
            // If a response is received then this is in response to single logout having been initiated by the identity provider.
            var sloResult = await _samlIdentityProvider.ReceiveSloAsync();

            if (sloResult.IsResponse)
            {
                if (sloResult.HasCompleted)
                {
                    // IdP-initiated SLO has completed.
                    if (!string.IsNullOrEmpty(sloResult.RelayState))
                    {
                        return LocalRedirect(sloResult.RelayState);
                    }

                    return RedirectToPage("/Index");
                }
            }
            else
            {
                // Logout locally.
                await _signInManager.SignOutAsync();

                // Respond to the SP-initiated SLO request indicating successful logout.
                await _samlIdentityProvider.SendSloAsync();
            }

            return new EmptyResult();
        }

        [Authorize]
        public async Task<IActionResult> SingleSignOnServiceCompletion()
        {
            await CompleteSsoAsync();

            return new EmptyResult();
        }

        private Task CompleteSsoAsync()
        {
            var tenantId = "idpfakesso";
            _samlIdentityProvider.SetConfigurationIDAsync(tenantId);

            var userName = User.Identity.Name;

            var attributes = SetConventionalSamlAttributes(userName, "", "SP_Initiated");

            // The user is logged in at the identity provider.
            // Respond to the authn request by sending a SAML response containing a SAML assertion to the SP.
            return _samlIdentityProvider.SendSsoAsync(userName, attributes);
        }

        private static List<SamlAttribute> SetConventionalSamlAttributes(string email, string targetUrl, string debugNotesOnSamlType)
        {
            var random = new Random();
            var lastNameThatUpdates = "LastNameThatChanges" + random.Next(100, 999);
            var firstName = email.Split('@')[0];

            var attributes = new List<SamlAttribute>()
            {
                new SamlAttribute("FIRST_NAME", firstName),
                new SamlAttribute("LAST_NAME", lastNameThatUpdates),
                new SamlAttribute("EMAIL", email),
                new SamlAttribute("BIRTHDATE", "12/18/1984"),
                new SamlAttribute("ANOTHER_THING", "whatever value"),
                new SamlAttribute("TARGET", targetUrl), //The return target page at the sp
                new SamlAttribute("DEBUG_SSO_TYPE", debugNotesOnSamlType),
            };
            return attributes;
        }
    }
}