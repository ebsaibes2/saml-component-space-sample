namespace Headspring.LocalDevSamlIdentityProvider
{
    using System.Collections.Generic;
    using ComponentSpace.Saml2.Configuration;
    using Data;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => false;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));
            services.AddDefaultIdentity<IdentityUser>()
                .AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddHttpContextAccessor();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // Use a unique identity cookie name rather than sharing the cookie across applications in the domain.
            services.ConfigureApplicationCookie(options =>
            {
                options.Cookie.Name = "ExampleIdentityProvider.Identity";
            });

            services.AddSaml(config => ConfigureSaml(config));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseAuthentication();

            app.UseMvc();
        }

        private void ConfigureSaml(SamlConfigurations samlConfigurations)
        {
            var samlConfigurationsConfigurations = new List<SamlConfiguration>();

            var ssoTenants = new List<string>
            {
                "idpfakesso",
            };

            foreach (var tenant in ssoTenants)
            {
                var samlConfiguration = new SamlConfiguration()
                {
                    ID = tenant,
                    LocalIdentityProviderConfiguration = new LocalIdentityProviderConfiguration()
                    {
                        Name = "urn:DevTestingIdentityProvider:" + tenant,
                        Description = "Identity Provider for " + tenant,
                        SingleSignOnServiceUrl = $"https://localhost:55999/Saml/SingleSignOnService",
                        LocalCertificates = new List<Certificate>()
                        {
                            new Certificate()
                            {
                                FileName = "certificates/idp.pfx",
                                Password = "password"
                            }
                        }
                    },
                    PartnerServiceProviderConfigurations = new List<PartnerServiceProviderConfiguration>()
                    {
                        new PartnerServiceProviderConfiguration()
                        {
                            Name = "urn:AppServiceProvider:FedGateway",
                            Description = "App Service Provider for FedGateway",
                            WantAuthnRequestSigned = true,
                            SignSamlResponse = true,
                            AssertionConsumerServiceUrl = $"https://localhost:8888/SAML/AssertionConsumerService",
                            SingleLogoutServiceUrl = $"https://localhost:8888/SAML/SingleLogoutService",
                            PartnerCertificates = new List<Certificate>()
                            {
                                new Certificate()
                                {
                                    FileName = "certificates/sp.cer",
                                    Use = "Encryption",
                                },
                                new Certificate()
                                {
                                    FileName = "certificates/sp.cer",
                                    Use = "Signature",
                                },
                            }
                        }
                    }
                };

                samlConfigurationsConfigurations.Add(samlConfiguration);
            }

            samlConfigurations.Configurations = samlConfigurationsConfigurations;
        }
    }
}
