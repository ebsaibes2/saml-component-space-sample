using Microsoft.AspNetCore.Mvc.RazorPages;

namespace MyeScreen.Pages
{
    public class LandingPageModel : PageModel
    {
        public bool IsUserAuthenticated { get; set; }

        public void OnGet()
        {
            IsUserAuthenticated = HttpContext.User.Identity.IsAuthenticated;
        }
    }
}
