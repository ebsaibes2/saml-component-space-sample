﻿using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Headspring.LocalDevSamlIdentityProvider.Pages
{
    public class LayoutModel: PageModel
    {
        public bool IsUserAuthenticated { get; set; }

        public void OnGet()
        {
            IsUserAuthenticated = HttpContext.User.Identity.IsAuthenticated;
        }
    }
}
