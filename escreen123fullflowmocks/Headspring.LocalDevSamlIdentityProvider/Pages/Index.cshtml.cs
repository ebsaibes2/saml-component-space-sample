﻿namespace Headspring.LocalDevSamlIdentityProvider.Pages
{
    using Microsoft.AspNetCore.Mvc.RazorPages;

    public class IndexModel : PageModel
    {
        public bool IsUserAuthenticated { get; set; }

        public void OnGet()
        {
            IsUserAuthenticated = HttpContext.User.Identity.IsAuthenticated;
        }
    }
}
