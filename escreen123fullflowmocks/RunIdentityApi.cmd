@echo off
rem Helper script for running the IDP outside the IDE

powershell -NoProfile -ExecutionPolicy Bypass -Command "dotnet run --project Headspring.LocalDevSamlIdentityProvider"
