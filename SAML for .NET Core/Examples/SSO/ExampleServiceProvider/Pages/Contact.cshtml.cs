﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ExampleServiceProvider.Pages
{
    [Authorize]
    public class ContactModel : PageModel
    {
        
        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [Required]
            public string FirstName { get; set; }

            [Required]
            public string LastName { get; set; }
        }

        public IActionResult OnGet(string firstName, string lastName)
        {
            Input = new InputModel {FirstName = firstName, LastName = lastName};

            return Page();
        }
    }
}
