﻿using ComponentSpace.Saml2;
using ComponentSpace.Saml2.Assertions;
using ComponentSpace.Saml2.XmlSecurity;
using Microsoft.Extensions.CommandLineUtils;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Xml;

namespace EncryptAssertion
{
    /// <summary>
    /// Encrypts SAML v2.0 assertions.
    /// 
    /// Usage: dotnet EncryptAssertion.dll <fileName> -c <certificateFileName> [-o <outputFileName>]
    /// 
    /// where the file contains a SAML assertion.
    /// 
    /// SAML assertions are encrypted using the public key associated with the X.509 certificate.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var commandLineApplication = new CommandLineApplication()
                {
                    Name = "EncryptAssertion",
                    Description = "Encrypts a SAML assertion"
                };

                commandLineApplication.HelpOption("-? | -h | --help");

                var fileNameArgument = commandLineApplication.Argument(
                    "fileName",
                    "The XML file containing a SAML assertion");

                var certificateOption = commandLineApplication.Option(
                    "-c | --certificate <certificateFileName>",
                    "The certificate file used to encrypt the assertion",
                    CommandOptionType.SingleValue);

                var outputOption = commandLineApplication.Option(
                    "-o | --output <outputFileName>",
                    "The generated XML file containing the encrypted assertion",
                    CommandOptionType.SingleValue);

                commandLineApplication.OnExecute(() =>
                {
                    if (string.IsNullOrEmpty(fileNameArgument.Value))
                    {
                        Console.WriteLine("The file name is missing.");
                        commandLineApplication.ShowHelp();

                        return -1;
                    }

                    if (string.IsNullOrEmpty(certificateOption.Value()))
                    {
                        Console.WriteLine("The certificate file name is missing.");
                        commandLineApplication.ShowHelp();

                        return -1;
                    }

                    EncryptAssertion(fileNameArgument.Value, certificateOption.Value(), outputOption.Value());

                    return 0;
                });

                commandLineApplication.Execute(args);
            }

            catch (Exception exception)
            {
                Console.WriteLine(exception.ToString());
            }
        }

        private static void EncryptAssertion(string fileName, string certificateFileName, string outputFileName)
        {
            if (!File.Exists(fileName))
            {
                throw new ArgumentException($"The file {fileName} doesn't exist.");
            }

            var xmlDocument = new XmlDocument
            {
                PreserveWhitespace = true
            };

            xmlDocument.Load(fileName);

            if (!File.Exists(certificateFileName))
            {
                throw new ArgumentException($"The certificate file {certificateFileName} doesn't exist.");
            }

            var serviceCollection = new ServiceCollection();

            serviceCollection.AddLogging();
            serviceCollection.AddSaml();

            var serviceProvider = serviceCollection.BuildServiceProvider();
            var xmlEncryption = serviceProvider.GetService<IXmlEncryption>();

            XmlElement encryptedDataElement = null;

            using (var x509Certificate = new X509Certificate2(certificateFileName))
            {
                using (var publicKey = x509Certificate.GetRSAPublicKey())
                {
                    encryptedDataElement = xmlEncryption.Encrypt(
                        xmlDocument.DocumentElement,
                        publicKey,
                        SamlConstants.KeyEncryptionAlgorithms.RSA_OAEP,
                        SamlConstants.DataEncryptionAlgorithms.AES_256,
                        x509Certificate);
                }
            }

            var encryptedAssertion = new EncryptedAssertion()
            {
                EncryptedData = encryptedDataElement
            };

            var encryptedAssertionElement = encryptedAssertion.ToXml();

            if (string.IsNullOrEmpty(outputFileName))
            {
                outputFileName = fileName;
            }

            encryptedAssertionElement.OwnerDocument.Save(outputFileName);
        }
    }
}
