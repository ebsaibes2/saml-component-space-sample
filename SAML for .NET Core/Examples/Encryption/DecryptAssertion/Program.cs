﻿using ComponentSpace.Saml2.Assertions;
using ComponentSpace.Saml2.XmlSecurity;
using Microsoft.Extensions.CommandLineUtils;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Xml;

namespace DecryptAssertion
{
    /// <summary>
    /// Decrypts SAML v2.0 assertions.
    /// 
    /// Usage: dotnet DecryptAssertion.dll <fileName> -c <certificateFileName> -p <password> [-o <outputFileName>]
    /// 
    /// where the file contains an encrypted SAML assertion.
    /// 
    /// SAML assertions are decrypted using the private key associated with the X.509 certificate.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var commandLineApplication = new CommandLineApplication()
                {
                    Name = "DecryptAssertion",
                    Description = "Decrypts an encrypted SAML assertion"
                };

                commandLineApplication.HelpOption("-? | -h | --help");

                var fileNameArgument = commandLineApplication.Argument(
                    "fileName",
                    "The XML file containing an encrypted SAML assertion");

                var certificateOption = commandLineApplication.Option(
                    "-c | --certificate <certificateFileName>",
                    "The certificate file used to decrypt the assertion",
                    CommandOptionType.SingleValue);

                var passwordOption = commandLineApplication.Option(
                    "-p | --password <password>",
                    "The certificate file password",
                    CommandOptionType.SingleValue);

                var outputOption = commandLineApplication.Option(
                    "-o | --output <outputFileName>",
                    "The generated XML file containing the decrypted assertion",
                    CommandOptionType.SingleValue);

                commandLineApplication.OnExecute(() =>
                {
                    if (string.IsNullOrEmpty(fileNameArgument.Value))
                    {
                        Console.WriteLine("The file name is missing.");
                        commandLineApplication.ShowHelp();

                        return -1;
                    }

                    if (string.IsNullOrEmpty(certificateOption.Value()))
                    {
                        Console.WriteLine("The certificate file name is missing.");
                        commandLineApplication.ShowHelp();

                        return -1;
                    }

                    if (string.IsNullOrEmpty(passwordOption.Value()))
                    {
                        Console.WriteLine("The certificate file password is missing.");
                        commandLineApplication.ShowHelp();

                        return -1;
                    }

                    DecryptAssertion(fileNameArgument.Value, certificateOption.Value(), passwordOption.Value(), outputOption.Value());

                    return 0;
                });

                commandLineApplication.Execute(args);
            }

            catch (Exception exception)
            {
                Console.WriteLine(exception.ToString());
            }
        }

        private static void DecryptAssertion(string fileName, string certificateFileName, string certificatePassword, string outputFileName)
        {
            if (!File.Exists(fileName))
            {
                throw new ArgumentException($"The file {fileName} doesn't exist.");
            }

            var xmlDocument = new XmlDocument
            {
                PreserveWhitespace = true
            };

            xmlDocument.Load(fileName);

            if (!File.Exists(certificateFileName))
            {
                throw new ArgumentException($"The certificate file {certificateFileName} doesn't exist.");
            }

            var serviceCollection = new ServiceCollection();

            serviceCollection.AddLogging();
            serviceCollection.AddSaml();

            var serviceProvider = serviceCollection.BuildServiceProvider();
            var xmlEncryption = serviceProvider.GetService<IXmlEncryption>();

            var encryptedAssertion = new EncryptedAssertion(xmlDocument.DocumentElement);

            XmlElement samlAssertionElement = null;

            using (var x509Certificate = new X509Certificate2(certificateFileName, certificatePassword))
            {
                using (var privateKey = x509Certificate.GetRSAPrivateKey())
                {
                    samlAssertionElement = xmlEncryption.Decrypt(
                        encryptedAssertion.EncryptedData,
                        encryptedAssertion.EncryptedKeys,
                        privateKey);
                }
            }

            if (string.IsNullOrEmpty(outputFileName))
            {
                outputFileName = fileName;
            }

            samlAssertionElement.OwnerDocument.Save(outputFileName);
        }
    }
}
