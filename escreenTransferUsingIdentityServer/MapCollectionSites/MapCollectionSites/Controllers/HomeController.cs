﻿using System;
using System.Collections.Generic;
using MapCollectionSites.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityModel;
using IdentityModel.Client;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Net.Http.Headers;

namespace MapCollectionSites.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            HttpContext.SignOutAsync("Cookies");
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> SignIn(string accessToken)
        {
            var client = new HttpClient();
            await TokenIntrospectionSignIn(accessToken, client);

            return RedirectToAction("Claims");
        }

        private async Task TokenIntrospectionSignIn(string accessToken, HttpClient client)
        {
            var response = await client.IntrospectTokenAsync(new TokenIntrospectionRequest
            {
                Address = "https://localhost:5200/connect/introspect",
                ClientId = "mapcollection",
                ClientSecret = "secret",
                Token = accessToken
            });


            if (response.IsError)
            {
                throw new UnauthorizedAccessException();
            }


            var claimsPrincipal =
                new ClaimsPrincipal(new ClaimsIdentity(response.Claims, CookieAuthenticationDefaults.AuthenticationScheme));
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsPrincipal));
        }

        private async Task TokenExchangeSignIn(string accessToken, HttpClient client)
        {
            var exchangeRepsonse = await client.RequestTokenExchangeTokenAsync(
                new TokenExchangeTokenRequest
                {
                    Address = "https://localhost:5200/connect/token",
                    ClientId = "mapcollection",
                    ClientSecret = "secret", 
                    SubjectToken = accessToken,
                    SubjectTokenType = OidcConstants.TokenTypes.AccessToken
                });
            if (exchangeRepsonse.IsError)
            {
                throw new UnauthorizedAccessException();
            }


            // var claimsPrincipal =
            //     new ClaimsPrincipal(new ClaimsIdentity(exchangeRepsonse.IdentityToken, CookieAuthenticationDefaults.AuthenticationScheme));
            // await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
            //     new ClaimsPrincipal(claimsPrincipal));
        }

        //[Authorize]
        [Authorize(Policy = "mapcollection")]
        public IActionResult Claims()
        {
            var user = HttpContext.User as ClaimsPrincipal;
            var claimsAndTokens = new ClaimAndTokensModel
            {
                Claims = new List<Claim>(),
                Tokens = new Dictionary<string, string>()
            };

            if (user.Claims.Any())
            {
                claimsAndTokens.Claims = user.Claims.ToList();
                claimsAndTokens.Tokens = new Dictionary<string, string>();
            }

            return View(claimsAndTokens);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
