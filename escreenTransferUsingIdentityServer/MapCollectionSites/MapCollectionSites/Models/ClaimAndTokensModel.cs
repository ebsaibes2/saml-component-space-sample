﻿using System.Collections.Generic;
using System.Security.Claims;

namespace MapCollectionSites.Models
{
    public class ClaimAndTokensModel
    {
        public  List<Claim> Claims { get; set; }
        public Dictionary<string, string> Tokens { get; set; }
    }
}