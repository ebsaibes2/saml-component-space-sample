using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using IdentityModel;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;

namespace MapCollectionSites
{
    public class Startup
    {
        private string identityAuthorityUrl = "https://localhost:5200";
        private string returnUrl = "https://localhost:44378/";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            JwtSecurityTokenHandler.DefaultMapInboundClaims = false;
            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = "oidc";
                })
                .AddCookie("Cookies")
                .AddOpenIdConnect("oidc", options =>
                {
                    options.SignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                    options.Authority = identityAuthorityUrl;

                    options.ClientId = "MapCollectionSites";
                    options.ClientSecret = "secret";
                    options.ResponseType = OidcConstants.ResponseTypes.CodeIdToken;
                    //options.ReturnUrlParameter = returnUrl;
                    options.GetClaimsFromUserInfoEndpoint = true;
                    options.Events.OnRedirectToIdentityProvider = context =>
                    {
                        context.ProtocolMessage.Prompt = "login";
                        return Task.CompletedTask;
                    }; 
                    options.SaveTokens = true;
                    options.Scope.Clear();
                    options.Scope.Add(OidcConstants.StandardScopes.OpenId);
                    options.Scope.Add(OidcConstants.StandardScopes.Profile);
                    //options.Scope.Add("mapcollection");

                    //This came from this post about missing claims
                    //https://leastprivilege.com/2017/11/15/missing-claims-in-the-asp-net-core-2-openid-connect-handler/
                    options.ClaimActions.Remove("amr");
                    options.ClaimActions.Remove("aud");
                    options.ClaimActions.Remove("azp");
                    options.ClaimActions.Remove("acr");
                    options.ClaimActions.Remove("amr");
                    options.ClaimActions.Remove("iss");
                    options.ClaimActions.Remove("iat");
                    options.ClaimActions.Remove("nbf");
                    options.ClaimActions.Remove("exp");

                    //map claims manually
                    options.ClaimActions.MapUniqueJsonKey("sub", "sub");
                    options.ClaimActions.MapUniqueJsonKey("name", "name");
                    options.ClaimActions.MapUniqueJsonKey("given_name", "given_name");
                    options.ClaimActions.MapUniqueJsonKey("family_name", "family_name");
                    options.ClaimActions.MapUniqueJsonKey("profile", "profile");
                    options.ClaimActions.MapUniqueJsonKey("mapcollection-user", "mapcollection-user");

                    //Access Denied Path
                    options.AccessDeniedPath = "/Home/Error";


                    options.Events = new OpenIdConnectEvents()
                    {
                        OnUserInformationReceived = context =>
                        {
                            string rawAccessToken = context.ProtocolMessage.AccessToken;
                            string rawIdToken = context.ProtocolMessage.IdToken;
                            var handler = new JwtSecurityTokenHandler();
                            var accessToken = handler.ReadJwtToken(rawAccessToken);
                            var idToken = handler.ReadJwtToken(rawIdToken);

                            // do something with the JWTs

                            return Task.CompletedTask;
                        },
                        OnRedirectToIdentityProvider = context =>
                        {
                            if(context.ProtocolMessage.RequestType == OpenIdConnectRequestType.Authentication)
                            context.ProtocolMessage.Prompt = "login";
                            return Task.CompletedTask;
                        }
                    };
                })
                .AddJwtBearer(options =>
                {
                    options.Authority = identityAuthorityUrl;
                    options.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateAudience = true,
                        ValidAudience = "mapcollection",
                    };
                    options.SaveToken = true;
                });

                services.AddAuthorization(options => 
                    options.AddPolicy("mapcollection", policy =>
                    {
                        policy.RequireAuthenticatedUser();
                        policy.RequireClaim("mapcollection-user");
                    }))
                ;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
