﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Catalog;
using IdentityServer3.Core;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.Owin;
using Microsoft.Owin.Extensions;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OpenIdConnect;
using Owin;

[assembly: OwinStartup(typeof(Catalog.Startup))]
namespace Catalog
{
    public class Startup
    {

        private readonly List<string> scope = new List<string>
        {
            Constants.StandardScopes.OpenId,
            Constants.StandardScopes.Profile,
            "mapcollection"
        };

        private static readonly string authorityUrl = "https://localhost:5200/";
        private static readonly string redirectUrl = "https://localhost:44328/";
        private static readonly string clientId = "Catalog";
        private static readonly string userInfoUrl = $"{authorityUrl}connect/userinfo";

        public void Configuration(IAppBuilder app)
        {

#if DEBUG
            ServicePointManager.SecurityProtocol =
                SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 |
                SecurityProtocolType.Tls
                ;

#endif
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = "Cookies",
            });
            app.UseOpenIdConnectAuthentication(new OpenIdConnectAuthenticationOptions
            {
                SignInAsAuthenticationType = "Cookies",
                Authority = authorityUrl,
                ClientId = clientId,
                ClientSecret = "secret",
                RedirectUri = redirectUrl,
                PostLogoutRedirectUri = redirectUrl,
                RedeemCode = true,
                Scope = string.Join(" ", scope),
                SaveTokens = true,
                UseTokenLifetime = true,
                ResponseType = Constants.ResponseTypes.CodeIdToken,
                Notifications = new OpenIdConnectAuthenticationNotifications
                {
                    AuthenticationFailed = authfailed =>
                    {
                        if (authfailed.Exception.Message.Contains("IDX21323"))
                        {
                            authfailed.HandleResponse();
                            authfailed.OwinContext.Authentication.Challenge("oidc");
                        }
                        return Task.FromResult(true);
                    },

                    RedirectToIdentityProvider = async n =>
                    {
                        if (n.ProtocolMessage.RequestType == OpenIdConnectRequestType.Authentication)
                        {
                            n.ProtocolMessage.RedirectUri = redirectUrl;
                            n.ProtocolMessage.Prompt = "login";
                        }

                        if (n.ProtocolMessage.RequestType == OpenIdConnectRequestType.Logout)
                        {
                            var result = await n.OwinContext.Authentication.AuthenticateAsync("Cookies");

                            var id_token = result.Properties.Dictionary.Where(x => x.Key == "id_token")
                                .Select(x => x.Value).FirstOrDefault();
                            
                            n.ProtocolMessage.IdTokenHint = id_token;
                        }
                        await Task.CompletedTask;
                    }
                }
            });
            app.UseStageMarker(PipelineStage.Authenticate);
        }
    }
}
