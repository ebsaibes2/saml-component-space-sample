﻿using System.Collections.Generic;
using System.IdentityModel.Claims;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Newtonsoft.Json.Linq;
using Claim = System.Security.Claims.Claim;

namespace Catalog.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        [Authorize]
        public ActionResult Claims()
        {
            ViewBag.Message = "Claims";
            var result = Request.GetOwinContext().Authentication.AuthenticateAsync("Cookies").Result;

            var model = new ClaimAndTokensModel
            {
                Claims = System.Security.Claims.ClaimsPrincipal.Current.Claims.ToList(),
                Tokens = new Dictionary<string, string>(result.Properties.Dictionary.Where(x=>x.Key.Contains("token")).ToDictionary(x=>x.Key,x=>x.Value)),
            };
   
            return View(model);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [Authorize]
        public ActionResult MapsCollectionSites()
        {
            return View();
        }

        [Authorize]
        public ActionResult Identity()
        {
            var authenticationResult = Request.GetOwinContext().Authentication.AuthenticateAsync("Cookies").Result;
            var accessToken = authenticationResult.Properties.Dictionary["access_token"];
            var MapsIdentityEndpoint = "https://localhost:44378/identityapi";
            
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            
            var content = client.GetStringAsync(MapsIdentityEndpoint).Result;
            ViewBag.Json = JArray.Parse(content).ToString();
            return View();
        }

        public ActionResult Logout()
        {
            Request.GetOwinContext().Authentication.SignOut();
            return Redirect("/");
        }
    }

    public class ClaimAndTokensModel
    {
        public  List<Claim> Claims { get; set; }
        public Dictionary<string, string> Tokens { get; set; }
    }
}