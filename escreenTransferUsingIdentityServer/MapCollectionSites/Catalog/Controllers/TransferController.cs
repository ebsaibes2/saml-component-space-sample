﻿using System;
using System.Web;
using System.Web.Mvc;
using Catalog.Models;
using IdentityModel.Client;

namespace Catalog.Controllers
{
    public class TransferController : Controller
    {
        private const string MapsUri = "https://localhost:44378/Home/SignIn";
        // GET
        public ActionResult RedirectToMaps()
        {
            var result = Request.GetOwinContext().Authentication.AuthenticateAsync("Cookies");

            var transferUrl = new Uri(MapsUri).ToString();
            var mapsModel = new MapsModel
            {
                TransferUrl = transferUrl,
                AccessToken = result.Result.Properties.Dictionary["access_token"]
            };
            return View(mapsModel);
        }
    }
}